package pl.mbrnwsk.minimax.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.border.TitledBorder;
import pl.mbrnwsk.minimax.Board;
import pl.mbrnwsk.minimax.Field;
import pl.mbrnwsk.minimax.BotPlayer;
import pl.mbrnwsk.minimax.HumanPlayer;
import pl.mbrnwsk.minimax.Player;

public class MainFrame extends JFrame {

    private JPanel contentPane;
    private BoardPanel boardPanel;
    private Field currentPlayer;
    private Board board;
    JButton btnBotWhiteMove;
    JButton btnBotBlackMove;

    /**
     * Create the frame.
     */
    public MainFrame() {
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initComponents();
        pack();
        setVisible(true);
    }

    private void initComponents() {
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu mnOptions = new JMenu("Options");
        menuBar.add(mnOptions);

        JMenuItem mntmNewGame = new JMenuItem("New game ");
        mntmNewGame.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setParameters();
            }
        });
        mnOptions.add(mntmNewGame);

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[][center]", "[center][]"));

        JPanel botsMovesPanel = new JPanel(new MigLayout());
        botsMovesPanel.setBorder(new TitledBorder("Bot's moves"));
        contentPane.add(botsMovesPanel, "cell 0 0 1 1");

        btnBotWhiteMove = new JButton("White's move");
        botsMovesPanel.add(btnBotWhiteMove);

        btnBotBlackMove = new JButton("Blacks's move");
        botsMovesPanel.add(btnBotBlackMove);

        JButton btnUndoMove = new JButton("Undo last move");
        contentPane.add(btnUndoMove, "cell 1 0 1 1");

        JPanel tempPanel = new JPanel(new MigLayout("", "[grow, center]",
                "[grow, center]"));
        tempPanel.add(new JLabel("Select Options -> New Game to start"));
        tempPanel.setPreferredSize(new Dimension(400, 400));
        contentPane.add(tempPanel, "cell 0 1 2 1");
    }

    private void setParameters() {
        Settings s = new Settings(this);
        s.setVisible(true);
        board = s.getBoard();
        boardPanel = new BoardPanel(board);
        Player blackPlayer = s.getBlackPlayer();
        Player whitePlayer = s.getWhitePlayer();
        s.dispose();
        initComponents();
        if (blackPlayer.isHuman()) {
            boardPanel.addMouseListener((HumanPlayer) blackPlayer);
        } else {
            btnBotBlackMove.addActionListener((BotPlayer) blackPlayer);
        }
        if (whitePlayer.isHuman()) {
            boardPanel.addMouseListener((HumanPlayer) whitePlayer);
        } else {
            btnBotWhiteMove.addActionListener((BotPlayer) whitePlayer);
        }
        contentPane.remove(2);
        contentPane.add(boardPanel, "cell 0 1 2 1");
        revalidate();
        currentPlayer = Field.BLACK;
    }

    public void play(int row, int column) {
        board.makeMove(currentPlayer, row, column, true);
        boardPanel.repaint();
        currentPlayer = currentPlayer.getOpponent();
    }

    public void skipTurn() {
        currentPlayer = currentPlayer.getOpponent();
    }

    public Field getCurrentPlayer() {
        return currentPlayer;
    }
}
