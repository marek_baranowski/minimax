package pl.mbrnwsk.minimax.gui;

import java.awt.Color;
import pl.mbrnwsk.minimax.Board;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import pl.mbrnwsk.minimax.*;

/**
 *
 * @author mbrnw_000
 */
public class BoardPanel extends JPanel {

    private Board board;

    public BoardPanel(Board board) {
        this.board = board;

        setPreferredSize(new java.awt.Dimension(400, 400));
        setBackground(new Color(Integer.parseInt("55CB4E", 16)));
        setLayout(null);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        drawGrid(g2d);
        drawFigures(g2d);
    }

    private void drawGrid(Graphics2D g) {
        for (int i = 1; i < board.getSize(); i++) {
            int x = (this.getWidth() / board.getSize()) * i;
            int y = (this.getHeight() / board.getSize()) * i;
            g.drawLine(0, y, this.getWidth(), y);
            g.drawLine(x, 0, x, this.getHeight());
        }
    }

    private void drawFigures(Graphics2D g) {
        int columnWidth = this.getWidth() / board.getSize();
        int rowHeight = this.getHeight() / board.getSize();

        int marginX = columnWidth / 10;
        int marginY = rowHeight / 10;

        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                Field player = board.getElementAt(i, j);
                if (player.getColor() != null) {
                    g.setColor(player.getColor());
                    int x = j * columnWidth + marginX;
                    int y = i * rowHeight + marginY;
                    int width = columnWidth - 2 * marginX;
                    int height = rowHeight - 2 * marginY;
                    g.fillOval(x, y, width, height);
                }
            }
        }
    }
}
