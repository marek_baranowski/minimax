package pl.mbrnwsk.minimax.gui;

import javax.swing.JDialog;
import net.miginfocom.swing.MigLayout;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import java.awt.CardLayout;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import pl.mbrnwsk.minimax.Board;
import pl.mbrnwsk.minimax.Field;
import pl.mbrnwsk.minimax.games.gomoku.GomokuBoard;
import pl.mbrnwsk.minimax.BotPlayer;
import pl.mbrnwsk.minimax.HumanPlayer;
import pl.mbrnwsk.minimax.Player;
import pl.mbrnwsk.minimax.games.reversi.ReversiBoard;

public class Settings extends JDialog {

    private Board board;
    private Player blackPlayer;
    private Player whitePlayer;

    /**
     * Create the dialog.
     */
    public Settings(final MainFrame parentFrame) {
        setModal(true);
        setLocationRelativeTo(parentFrame);
        getContentPane().setLayout(new MigLayout("", "[][grow]", "[grow][]"));

        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(null, "Game", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        getContentPane().add(panel, "cell 0 0,grow");
        panel.setLayout(new MigLayout("", "[][]", "[][]"));

        final JComboBox comboBox = new JComboBox();
        comboBox.setModel(new DefaultComboBoxModel(new String[]{"Gomoku", "Reversi"}));
        panel.add(comboBox, "cell 0 0 2 1,growx");

        JLabel lblSize = new JLabel("size");
        panel.add(lblSize, "cell 0 1");

        final JSpinner size = new JSpinner();
        size.setModel(new SpinnerNumberModel(new Integer(19), new Integer(1), null, new Integer(1)));
        panel.add(size, "cell 1 1,growx");

        comboBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (comboBox.getSelectedItem().equals("Gomoku")) {
                    size.setValue(19);
                } else {
                    size.setValue(8);
                }
            }
        });

        JPanel panel_1 = new JPanel();
        panel_1.setBorder(new TitledBorder(null, "Players",
                TitledBorder.LEADING, TitledBorder.TOP, null, null));
        getContentPane().add(panel_1, "cell 1 0,grow");
        panel_1.setLayout(new MigLayout("", "[grow][grow]", "[grow]"));

        JPanel panel_2 = new JPanel();
        panel_2.setBorder(new TitledBorder(null, "Black",
                TitledBorder.LEADING, TitledBorder.TOP, null, null));
        panel_1.add(panel_2, "cell 0 0,grow");
        panel_2.setLayout(new MigLayout("", "[grow]", "[][grow]"));

        final JComboBox blackPlayerCB = new JComboBox();
        blackPlayerCB.setModel(new DefaultComboBoxModel(
                new String[]{"Bot", "Human"}));
        panel_2.add(blackPlayerCB, "cell 0 0,growx");

        final JPanel panel_4 = new JPanel();
        panel_2.add(panel_4, "cell 0 1,grow");
        panel_4.setLayout(new CardLayout(0, 0));

        blackPlayerCB.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                CardLayout cl = (CardLayout) panel_4.getLayout();
                cl.show(panel_4, (String) e.getItem());
            }
        });

        JPanel blackBotPanel = new JPanel();
        panel_4.add(blackBotPanel, "Bot");
        blackBotPanel.setLayout(new MigLayout("", "[grow][grow]", "[][]"));

        JLabel lblDepth = new JLabel("depth");
        blackBotPanel.add(lblDepth, "cell 0 0");

        final JSpinner blackDepth = new JSpinner();
        blackDepth.setModel(new SpinnerNumberModel(new Integer(3),
                new Integer(1), null, new Integer(1)));
        blackBotPanel.add(blackDepth, "cell 1 0,growx");

        final JCheckBox blackAlfaBeta = new JCheckBox("α-β pruning");
        blackBotPanel.add(blackAlfaBeta, "cell 0 1 2 1");

        JPanel blackHumanPanel = new JPanel();
        panel_4.add(blackHumanPanel, "Human");

        JPanel panel_6 = new JPanel();
        panel_6.setBorder(new TitledBorder(
                UIManager.getBorder("TitledBorder.border"),
                "White", TitledBorder.LEADING, TitledBorder.TOP, null, null));
        panel_1.add(panel_6, "cell 1 0,grow");
        panel_6.setLayout(new MigLayout("", "[grow]", "[][grow]"));

        final JComboBox whitePlayerCB = new JComboBox();
        whitePlayerCB.setModel(new DefaultComboBoxModel(
                new String[]{"Bot", "Human"}));
        panel_6.add(whitePlayerCB, "cell 0 0,growx");

        final JPanel panel_7 = new JPanel();
        panel_6.add(panel_7, "cell 0 1,grow");
        panel_7.setLayout(new CardLayout(0, 0));

        whitePlayerCB.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                CardLayout cl = (CardLayout) panel_7.getLayout();
                cl.show(panel_7, (String) e.getItem());
            }
        });

        JPanel whiteBotPanel = new JPanel();
        panel_7.add(whiteBotPanel, "Bot");
        whiteBotPanel.setLayout(new MigLayout("", "[grow][grow]", "[][]"));

        JLabel label = new JLabel("depth");
        whiteBotPanel.add(label, "cell 0 0");

        final JSpinner whiteDepth = new JSpinner();
        whiteDepth.setModel(new SpinnerNumberModel(new Integer(3),
                new Integer(1), null, new Integer(1)));
        whiteBotPanel.add(whiteDepth, "cell 1 0,growx");

        final JCheckBox whiteAlfaBeta = new JCheckBox("α-β pruning");
        whiteBotPanel.add(whiteAlfaBeta, "cell 0 1 2 1");

        JPanel whiteHumanPanel = new JPanel();
        panel_7.add(whiteHumanPanel, "Human");

        JButton btnNewButton = new JButton("Start new game");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (comboBox.getSelectedItem().equals("Gomoku")) {
                    board = new GomokuBoard((Integer) size.getValue());
                } else {
                    board = new ReversiBoard((Integer) size.getValue());
                }
                int depth = (Integer) blackDepth.getValue();
                boolean alfaBeta = blackAlfaBeta.isSelected();
                if (blackPlayerCB.getSelectedItem().equals("Bot")) {
                    blackPlayer = new BotPlayer(board, depth, alfaBeta,
                            Field.BLACK, parentFrame);
                } else {
                    blackPlayer = new HumanPlayer(board, Field.BLACK,
                            parentFrame);
                }

                depth = (Integer) whiteDepth.getValue();
                alfaBeta = whiteAlfaBeta.isSelected();

                if (whitePlayerCB.getSelectedItem().equals("Bot")) {
                    whitePlayer = new BotPlayer(board, depth, alfaBeta,
                            Field.WHITE, parentFrame);
                } else {
                    whitePlayer = new HumanPlayer(board, Field.WHITE,
                            parentFrame);
                }
                
                setVisible(false);
            }
        });
        getContentPane().add(btnNewButton, "cell 0 1 2 1,alignx center");
        pack();
    }
    
    public Board getBoard() {
        return board;
    }
    
    public Player getBlackPlayer() {
        return blackPlayer;
    }

    public Player getWhitePlayer() {      
        return whitePlayer;
    }

}
