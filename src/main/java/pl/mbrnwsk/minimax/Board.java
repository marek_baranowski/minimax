package pl.mbrnwsk.minimax;

/**
 *
 * @author mbrnw_000
 */
public interface Board {

    int getSize();

    boolean gameOver();
    
    boolean isGameStarted();

    int score(int whichMethod);

    Field getElementAt(int row, int column);

    boolean isValidMove(Field player, int row, int column);

    void makeMove(Field player, int row, int column, boolean isDefinitiveMove);

    void undoLastMove();

    Board clone();
}
