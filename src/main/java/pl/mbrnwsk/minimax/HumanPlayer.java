package pl.mbrnwsk.minimax;

import pl.mbrnwsk.minimax.Player;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;
import pl.mbrnwsk.minimax.Board;
import pl.mbrnwsk.minimax.Field;
import pl.mbrnwsk.minimax.gui.MainFrame;

/**
 *
 * @author mbrnw_000
 */
public class HumanPlayer implements Player, MouseListener {

    private Board board;
    private Field piece;
    private MainFrame gameInterface;

    public HumanPlayer(Board board, Field piece,
            MainFrame gameInterface) {
        this.board = board;
        this.piece = piece;
        this.gameInterface = gameInterface;
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
        if (!gameInterface.getCurrentPlayer().equals(piece)) {
            return;
        }
        
        if (!hasValidMoves()) {
            System.out.println("skit");
            gameInterface.skipTurn();
        }
        
        JPanel source = (JPanel) e.getSource();
        int row = (e.getY() * board.getSize()) / source.getHeight();
        int column = (e.getX() * board.getSize()) / source.getWidth();
        
        if (board.isValidMove(piece, row, column)) {
            gameInterface.play(row, column);
        }
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    private boolean hasValidMoves() {
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                if (board.isValidMove(piece, i, j)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isHuman() {
        return true;
    }
}
