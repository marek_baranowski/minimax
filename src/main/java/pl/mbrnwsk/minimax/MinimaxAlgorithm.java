package pl.mbrnwsk.minimax;

public class MinimaxAlgorithm {

    private int depth;
    private boolean alfaBeta;

    public MinimaxAlgorithm(int depth, boolean alfaBeta) {
        this.depth = depth;
        this.alfaBeta = alfaBeta;
    }

    public int getDepth() {
        return depth;
    }

    /**
     * The recursive minimax function
     *
     * @param max indicates whether to find maximum or minimum value
     * @return
     */
    private int minimax(Board board, Field player, boolean max, int depth) {
        int score = player.equals(Field.BLACK) ? board.score(1) : board.score(2);
        if (board.gameOver() || depth == 0) {
            return score;
        }
        int bestScore = max ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        for (int row = 0; row < board.getSize(); row++) {
            for (int column = 0; column < board.getSize(); column++) {
                if (board.isValidMove(player, row, column)) {
                    Board boardClone = board.clone();
                    boardClone.makeMove(player, row, column, false);
                    score = minimax(boardClone, player.getOpponent(), !max, depth - 1);
                    if (max) {
                        bestScore = Math.max(bestScore, score);
                    } else {
                        bestScore = Math.min(bestScore, score);
                    }
                }
            }
        }
        return bestScore;
    }

    private int alfaBeta(Board board, Field player, boolean max,
            int depth, int alfa) {
        int score = player.equals(Field.BLACK) ? board.score(1) : board.score(2);
        if (board.gameOver() || depth == 0) {
            return score;
        }
        int bestScore = max ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        loop:
        for (int row = 0; row < board.getSize(); row++) {
            for (int column = 0; column < board.getSize(); column++) {
                if (board.isValidMove(player, row, column)) {
                    Board boardClone = board.clone();
                    boardClone.makeMove(player, row, column, false);
                    score = alfaBeta(boardClone, player.getOpponent(),
                            !max, depth - 1, bestScore);
                    if (max) {
                        bestScore = Math.max(bestScore, score);
                        if (bestScore >= alfa) {
                            break loop;
                        }
                    } else {
                        bestScore = Math.min(bestScore, score);
                        if (bestScore <= alfa) {
                            break loop;
                        }
                    }
                }
            }
        }
        return bestScore;
    }

    /**
     * Find the best move for X and play it on the board.
     */
    public Coord findBestMove(Board board, Field player, boolean max) {
        int score;
        int bestScore = max ? Integer.MIN_VALUE : Integer.MAX_VALUE;
        Integer bestRow = null;
        Integer bestColumn = null;
        for (int row = 0; row < board.getSize(); row++) {
            for (int column = 0; column < board.getSize(); column++) {
                if (board.isValidMove(player, row, column)) {
                    Board boardClone = board.clone();
                    boardClone.makeMove(player, row, column, false);
                    if (alfaBeta) {
                        score = alfaBeta(boardClone, player.getOpponent(),
                                !max, depth - 1, bestScore);
                    } else {
                        score = minimax(boardClone, player.getOpponent(),
                                !max, depth - 1);
                    }
                    if (max) {
                        if (score >= bestScore) {
                            bestScore = score;
                            bestRow = row;
                            bestColumn = column;
                        }
                    } else {
                        if (score <= bestScore) {
                            bestScore = score;
                            bestRow = row;
                            bestColumn = column;
                        }
                    }

                }
            }
        }
        System.out.println(player + ", bestScore: " + bestScore);
        return new Coord(bestRow, bestColumn);
    }
}
