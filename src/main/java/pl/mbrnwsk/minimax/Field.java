package pl.mbrnwsk.minimax;

import java.awt.Color;

/**
 *
 * @author mbrnw_000
 */
public enum Field {

    BLACK(Color.BLACK),
    WHITE(Color.WHITE),
    EMPTY(null);
    
    private final Color color;
    
    private Field(Color color) {
        this.color = color;
    }

    public Field getOpponent() {
        return this.equals(BLACK) ? WHITE : BLACK;
    }

    public Color getColor() {
        return color;
    }

}
