package pl.mbrnwsk.minimax;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import pl.mbrnwsk.minimax.games.gomoku.GomokuBoard;
import pl.mbrnwsk.minimax.gui.MainFrame;

/**
 *
 * @author mbrnw_000
 */
public class BotPlayer implements Player, ActionListener {

    private MinimaxAlgorithm minimax;
    private Board board;
    private Field piece;
    private MainFrame gameInterface;
    private boolean max;

    public BotPlayer(Board board, int depth, boolean alfaBeta, Field piece,
            MainFrame gameInterface) {
        this.board = board;
        this.piece = piece;
        this.gameInterface = gameInterface;
        minimax = new MinimaxAlgorithm(depth, true);
        max = piece.equals(Field.BLACK) ? true : false;
    }

    public void actionPerformed(ActionEvent e) {
        System.out.println(gameInterface.getCurrentPlayer());
        if (!gameInterface.getCurrentPlayer().equals(piece)) {
            return;
        }
        Coord bestMove;
        if (board instanceof GomokuBoard && !board.isGameStarted()) {
                bestMove = new Coord(board.getSize() / 2, board.getSize() / 2);
        } else {
            long time = System.currentTimeMillis();
            bestMove = minimax.findBestMove(board, piece, max);
            System.out.println(System.currentTimeMillis() - time);
        }
        //bestMove = minimax.findBestMove(board, piece, max);
        if (bestMove.getX() != null && bestMove.getY() != null) {
            gameInterface.play(bestMove.getX(), bestMove.getY());
        } else {
            gameInterface.skipTurn();
        }
    }

    public boolean isHuman() {
        return false;
    }
}
