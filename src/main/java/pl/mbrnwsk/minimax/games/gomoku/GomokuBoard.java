package pl.mbrnwsk.minimax.games.gomoku;

import pl.mbrnwsk.minimax.Board;
import java.util.LinkedList;
import java.util.List;
import pl.mbrnwsk.minimax.Coord;
import pl.mbrnwsk.minimax.Field;

/**
 *
 * @author mbrnw_000
 */
public class GomokuBoard implements Board {

    private Field[] board;
    private int size;
    private Coord[] coordinates = new Coord[]{new Coord(-1, -1), new Coord(-1, 0),
        new Coord(-1, 1), new Coord(0, -1), new Coord(0, 1),
        new Coord(1, -1), new Coord(1, 0), new Coord(1, 1)};
    private Field lastPlayer;
    private List<Coord> lastChanges = new LinkedList<Coord>();
    private int lastScore = 0;
    private boolean gameStarted = false;

    public GomokuBoard(int size) {
        this.size = size;
        board = new Field[size * size];
        initBoard();
        //score();
    }

    public GomokuBoard(Field[] board, int size) {
        this.board = board;
        this.size = size;
    }

    private void initBoard() {
        for (int i = 0; i < board.length; i++) {
            board[i] = Field.EMPTY;
        }
    }

    public int getSize() {
        return size;
    }

    public Field getElementAt(int row, int col) {
        return board[row * size + col];
    }

    public void setElementAt(Field player, int row, int col) {
        board[row * size + col] = player;
    }

    public boolean isValidMove(Field player, int row, int column) {
        if (gameStarted == false) {
            return true;
        }
        if (!getElementAt(row, column).equals(Field.EMPTY)) {
            return false;
        }
        for (Coord c : coordinates) {
            int x = row + c.getX();
            int y = column + c.getY();
            if (onBoard(x, y)) {
                if (!getElementAt(x, y).equals(Field.EMPTY)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void makeMove(Field player, int row, int column, boolean isDefinitiveMove) {
        if (isDefinitiveMove) {
            lastChanges.clear();
            lastChanges.add(new Coord(row, column));
            lastPlayer = player;
        }
        setElementAt(player, row, column);
        if (gameStarted == false) {
            gameStarted = true;
        }
    }

    public void undoLastMove() {
    }

    public boolean gameOver() {
        return lastScore >= 1000 || lastScore <= -1000;
    }

    public int score(int whichMethod) {
        //setElementAt(Field.WHITE, 5, 5);
//        setElementAt(Field.WHITE, 6, 6);
//        setElementAt(Field.WHITE, 7, 7);
//        setElementAt(Field.WHITE, 8, 8);
//        setElementAt(Field.BLACK, 8, 9);
//        setElementAt(Field.BLACK, 8, 10);
//        setElementAt(Field.BLACK, 8, 11);
//        setElementAt(Field.BLACK, 8, 12);
        
        int result = evaluateBoard(Field.BLACK) - evaluateBoard(Field.WHITE);
        //System.out.println(result);
        return result;
    }

    int evaluateBoard(Field field) {
        int score = 0;
        for (int i = 0; i < size; i++) {
            char[] row = new char[size];
            getLine(i, 0, 0, 1, row, 0, field);
            score += evaluateRow(new String(row));

            char[] column = new char[size];
            getLine(0, i, 1, 0, column, 0, field);
            score += evaluateRow(new String(column));

            if (i < size - 4) {
                char[] diagonal1Down = new char[size - i];
                getLine(i, 0, 1, 1, diagonal1Down, 0, field);
                score += evaluateRow(new String(diagonal1Down));

                char[] diagonal2Down = new char[size - i];
                getLine(i, size - 1, 1, -1, diagonal2Down, 0, field);
                score += evaluateRow(new String(diagonal2Down));

                if (i != size - 5) {
                    char[] diagonal1Up = new char[size - i - 1];
                    getLine(0, i + 1, 1, 1, diagonal1Up, 0, field);
                    score += evaluateRow(new String(diagonal1Up));

                    char[] diagonal2Up = new char[size - i - 1];
                    getLine(0, size - 2 - i, 1, -1, diagonal2Up, 0, field);
                    score += evaluateRow(new String(diagonal2Up));
                }
            }
        }
        return score;
    }

    public void getLine(int row, int column, int x, int y, char[] seq, int pos, Field player) {
        if (!onBoard(row, column)) {
            return;
        }
        if (getElementAt(row, column).equals(Field.EMPTY)) {
            seq[pos] = '_';
        } else if (getElementAt(row, column).equals(player)) {
            seq[pos] = 'G'; // good
        } else {
            seq[pos] = 'B'; // bad
        }
        getLine(row + x, column + y, x, y, seq, pos + 1, player);
    }

    private int evaluateRow(String row) {
        int score = 0;
        if (row.contains("GGGGG")) {
            return 1000;
        }
        if (row.contains("_GGGG_")) {
            return 100;
        }
        if (row.contains("_GG_G_")) {
            score += 5;
        }
        if (row.contains("_G_GG_")) {
            score += 5;
        }
        if (row.contains("_GGG_")) {
            score += 5;
        }
        if (row.contains("_G_G_")) {
            score += 2;
        }
        if (row.contains("GGGG_")) {
            score += 2;
        }
        if (row.contains("_GGGG")) {
            score += 2;
        }
        if (row.contains("_GG_")) {
            score += 1;
        }
        return score;
    }

    public boolean onBoard(int row, int col) {
        return row > -1 && row < size && col > -1 && col < size;
    }

    @Override
    public Board clone() {
        return new GomokuBoard(board.clone(), size);
    }

    public boolean isGameStarted() {
        return gameStarted;
    }
}
