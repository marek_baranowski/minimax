package pl.mbrnwsk.minimax.games.reversi;

import java.util.LinkedList;
import java.util.List;
import pl.mbrnwsk.minimax.Board;
import pl.mbrnwsk.minimax.Coord;
import pl.mbrnwsk.minimax.Field;

/**
 *
 * @author mbrnw_000
 */
public class ReversiBoard implements Board {

    private Field[] board;
    private int size;
    private Coord[] coordinates = new Coord[]{new Coord(-1, -1), new Coord(-1, 0),
        new Coord(-1, 1), new Coord(0, -1), new Coord(0, 1),
        new Coord(1, -1), new Coord(1, 0), new Coord(1, 1)};
    private Field lastPlayer;
    private List<Coord> lastChanges = new LinkedList<Coord>();

//    public static void main(String args[]){
//        new ReversiBoard(8);
//    }
    public ReversiBoard(int size) {
        this.size = size;
        board = new Field[size * size];
        initBoard();
//        setElementAt(Field.BLACK, 2, 7);
//        System.out.println(grantPositions(Field.BLACK));
    }

    public ReversiBoard(Field[] board, int size) {
        this.board = board;
        this.size = size;
    }

    private void initBoard() {
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                setElementAt(Field.EMPTY, row, col);
            }
        }
        setElementAt(Field.WHITE, 3, 3);
        setElementAt(Field.WHITE, 4, 4);
        setElementAt(Field.BLACK, 3, 4);
        setElementAt(Field.BLACK, 4, 3);
    }

    public int getSize() {
        return size;
    }

    public Field getElementAt(int row, int col) {
        return board[row * size + col];
    }

    public void setElementAt(Field player, int row, int col) {
        board[row * size + col] = player;
    }

    public boolean gameOver() {
        //In reversi, game is over when both players can't make move
        return false;
    }

    public void makeMove(Field player, int row, int col, boolean isDefinitiveMove) {
        if (isDefinitiveMove) {
            lastChanges.clear();
            lastChanges.add(new Coord(row, col));
            lastPlayer = player;
        }
        setElementAt(player, row, col);
        flip(player, row, col, isDefinitiveMove);
    }

    public boolean isValidMove(Field player, int row, int col) {
        if (!getElementAt(row, col).equals(Field.EMPTY)) {
            return false;
        }
        for (Coord c : coordinates) {
            int x = row + c.getX();
            int y = col + c.getY();
            if (onBoard(x, y)) {
                if (getElementAt(x, y).equals(player.getOpponent())) {
                    x += c.getX();
                    y += c.getY();
                    while (onBoard(x, y)) {
                        if (getElementAt(x, y).equals(Field.EMPTY)) {
                            break;
                        }
                        if (getElementAt(x, y).equals(player)) {
                            return true;
                        }
                        x += c.getX();
                        y += c.getY();
                    }
                }
            }
        }
        return false;
    }

    private void flip(Field player, int row, int col, boolean isDefinitiveMove) {
        for (Coord c : coordinates) {
            int x = row + c.getX();
            int y = col + c.getY();
            if (onBoard(x, y)) {
                if (getElementAt(x, y).equals(player.getOpponent())) {
                    x += c.getX();
                    y += c.getY();
                    while (onBoard(x, y)) {
                        if (getElementAt(x, y).equals(Field.EMPTY)) {
                            break;
                        }
                        if (getElementAt(x, y).equals(player)) {
                            x -= c.getX();
                            y -= c.getY();
                            while (!(x == row && y == col)) {
                                setElementAt(player, x, y);
                                if (isDefinitiveMove) {
                                    lastChanges.add(new Coord(x, y));
                                }
                                x -= c.getX();
                                y -= c.getY();
                            }
                            break;
                        }
                        x += c.getX();
                        y += c.getY();
                    }
                }
            }
        }
    }

    public int score(int which) {
        if(which == 1){
            return countPieces();
        }
        else {
            return grantPositions(Field.BLACK) - grantPositions(Field.WHITE);
        }
    }

    private int countPieces() {
        int blacks = 0;
        int whites = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (getElementAt(i, j).equals(Field.BLACK)) {
                    blacks++;
                } else if (getElementAt(i, j).equals(Field.WHITE)) {
                    whites++;
                }
            }
        }
        return blacks - whites;
    }

    public int grantPositions(Field field) {
        int count = 0;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (getElementAt(i, j).equals(field)) {
                    if (i == 0 && j == 0 || i == 7 && j == 7
                            || i == 0 && j == 7 || i == 7 && j == 0) {
                        count += 60;
                    } else if (i == 0 && j == 1 || i == 1 && j == 0
                            || i == 0 && j == 6 || i == 6 && j == 0
                            || i == 7 && j == 6 || i == 6 && j == 7
                            || i == 6 && j == 0 || i == 7 && j == 1) {
                        count += -30;
                    } else if (i == 1 && j == 1 || i == 1 && j == 6
                            || i == 6 && j == 1 || i == 6 && j == 6) {
                        count += -40;
                    } else if ((i > 1 && i < 6 && j == 0)
                            || (i > 1 && i < 6 && j == 7)
                            || (j > 1 && j < 6 && i == 0)
                            || (j > 1 && j < 6 && i == 7)) {
                        count += 25;
                    } else if ((i > 1 && i < 6 && j == 1)
                            || (i > 1 && i < 6 && j == 6)
                            || (j > 1 && j < 6 && i == 1)
                            || (j > 1 && j < 6 && i == 6)) {
                        count += -25;
                    } else {
                        count += 1;
                    }
                }
            }
        }
        return count;
    }

    public boolean onBoard(int row, int col) {
        return row > -1 && row < size && col > -1 && col < size;
    }

    public void undoLastMove() {
        for (int i = 0; i < lastChanges.size(); i++) {
            Coord c = lastChanges.get(i);
            if (i == 0) {
                setElementAt(Field.EMPTY, c.getX(), c.getY());
            } else {
                setElementAt(lastPlayer.getOpponent(), c.getX(), c.getY());
            }
        }
    }

    @Override
    public ReversiBoard clone() {
        return new ReversiBoard(board.clone(), size);
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (getElementAt(i, j).equals(Field.BLACK)) {
                    result += "X ";
                } else if (getElementAt(i, j).equals(Field.WHITE)) {
                    result += "O ";
                } else if (getElementAt(i, j).equals(Field.EMPTY)) {
                    result += "_ ";
                }
            }
            result += "\n";
        }
        return result;
    }

    public boolean isGameStarted() {
        return true;
    }
}
