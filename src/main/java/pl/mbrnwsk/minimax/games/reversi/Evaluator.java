package pl.mbrnwsk.minimax.games.reversi;

import pl.mbrnwsk.minimax.Board;

/**
 *
 * @author mbrnw_000
 */
public interface Evaluator {
    
    int evaluate(Board board);
    
}
