package pl.mbrnwsk.minimax.games.reversi;

import pl.mbrnwsk.minimax.Board;
import pl.mbrnwsk.minimax.Field;

/**
 *
 * @author mbrnw_000
 */
public class CountEvalutaor implements Evaluator {

    public int evaluate(Board board) {
        int blacks = 0;
        int whites = 0;
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                if (board.getElementAt(i, j).equals(Field.BLACK)) {
                    blacks++;
                } else if (board.getElementAt(i, j).equals(Field.WHITE)) {
                    whites++;
                }
            }
        }
        return blacks - whites;
    }
}
